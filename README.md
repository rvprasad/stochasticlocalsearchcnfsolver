# StochasticLocalSearchCNFSolver

A tool to solve non-trivial CNF formulae (i.e., with at least one clause) leveraging the algorithm W (WalkSAT) as described in [Handbook of Satisfiability](https://www.iospress.nl/book/handbook-of-satisfiability/) (pp189) and proposed in [Local search strategies for satisfiability testing](http://www.cs.rochester.edu/u/kautz/papers/dimacs93.ps) by *Bart Selman, Henry Kautz,* and *Bram Cohen*.  

The tool provides information about the number of seen/expected variables and seen/expected clauses along with some info about the runtime behavior of the underlying algorithm.  The implementation of the tool in different languages is available in corresponding folder -- _Go_ and _Kotlin_.


## Attribution

Copyright (c) 2017, Venkatesh-Prasad Ranganath

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
