/*
 * Copyright (c) 2019 Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@file:CompilerOpts("-jvm-target 1.8")
@file:DependsOn("com.github.ajalt:clikt:2.8.0")
//@file:KotlinOpts("-J-ea")

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.float
import com.github.ajalt.clikt.parameters.types.int
import java.io.File
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.IntStream
import kotlin.math.abs
import kotlin.math.absoluteValue
import kotlin.text.Regex

private typealias Assignment = Set<Int>
private typealias Clause = List<Int>
private typealias Var = UInt

internal class CommandLine: CliktCommand(name = "kscript solvecnf.kts",
        printHelpOnEmptyArgs = true,
        help = "WalkSAT algorithm based CNF solver") {
    private val cnf by option("-c", "--cnf", help = "CNF file in DIMACS format " +
            "(http://www.satcompetition.org/2009/format-benchmarks2009.html)").required()
    private val maxFlips by option("-f", "--maxFlips",
            help = "Maximum number of flips.  (Default: #vars * #clauses)").int()
    private val maxTries by option("-t", "--maxTries", help = "Maximum number of allowed tries/searches.  " +
            "(Default: #clauses)").int()
    private val noise by option("-n", "--noise", help="Noise parameter (real number between 0 and 1).  " +
            "(Default: 0.57)").float().default(0.57f)

    override fun run() {
        val (clauses, vars) = readClauses(cnf)
        val mFlips = maxFlips?.absoluteValue ?: vars.size * clauses.size
        val mTries = maxTries?.absoluteValue ?: clauses.size
        WalkSATCNFSolver.solve(clauses, vars, mFlips, mTries, noise)
                .let { (tries, flips, values) ->
            println("c tries $tries")
            println("c flips $flips")
            if (values.isEmpty()) {
                println("s UNSATISFIABLE")
            } else {
                println("s SATISFIABLE")
                println("v ${values.sortedBy(::abs).joinToString(" ")} 0")
            }
        }
    }

    private fun readClauses(fileName: String): Pair<Collection<Clause>, Set<Var>> {
        var expectedNumVars = 0
        var expectedNumClauses = 0
        val infoRegex = Regex("^p cnf \\d+ \\d+.*")
        val clauseRegex = Regex("^\\s*-?\\d.*")
        fun tokenize(line: String) = line.trim().split(' ').filter { it.isNotBlank() }

        val clauses = File(fileName).bufferedReader().lineSequence().map { line ->
            val tokens = tokenize(line)
            when {
                infoRegex.containsMatchIn(line) -> {
                    expectedNumVars = Integer.parseInt(tokens[2])
                    expectedNumClauses = Integer.parseInt(tokens[3])
                    null
                }
                clauseRegex.containsMatchIn(line) -> tokens.map { it.toInt() }.takeWhile { it != 0 }.toMutableList()
                else -> null
            }
        }.filterNotNull().toList()

        val seenVars = clauses.flatten().map { it.absoluteValue.toUInt() }.toSet()
        println("c Variables ${seenVars.size} seen $expectedNumVars expected")
        println("c Clauses ${clauses.size} seen $expectedNumClauses expected")

        return Pair(clauses, seenVars)
    }
}

private object WalkSATCNFSolver {
    private fun simplifyClausesBasedOnValues(clauses: Collection<Clause>, assignment: Assignment) =
            clauses.filterNot { c -> c.any { it in assignment } }.map { c -> c.filterNot { -it in assignment } }

    sealed class Verdict {
        class Satisfied(val assignment: Assignment): Verdict()
        object Conflicts: Verdict()
        class Unresolved(val clauses: Collection<Clause>, val partialAssignment: Assignment): Verdict()
    }

    private tailrec fun performBCP(clauses: Collection<Clause>, assignment: Assignment = emptySet()): Verdict {
        val valsFromUnitClauses = assignment + clauses.filter { it.size == 1 }.map { it[0] }.toSet()
        val simplifiedClauses = simplifyClausesBasedOnValues(clauses, valsFromUnitClauses)
        val newAssignment = assignment + valsFromUnitClauses
        return when {
            newAssignment.any { -it in newAssignment } || simplifiedClauses.any { it.isEmpty() } -> Verdict.Conflicts
            simplifiedClauses.isEmpty() -> Verdict.Satisfied(newAssignment)
            clauses.size != simplifiedClauses.size -> performBCP(simplifiedClauses, newAssignment)
            else -> Verdict.Unresolved(simplifiedClauses, newAssignment)
        }
    }
    private val random: Random
            get() = ThreadLocalRandom.current()

    fun solve(givenClauses: Collection<Clause>, givenVars: Set<Var>, maxFlips: Int, maxTries: Int, noise: Float):
            Triple<Int, Int, Assignment> {
        val verdict = performBCP(givenClauses.map { it.distinct() }, emptySet())
        val unresolved = when (verdict) {
            is Verdict.Conflicts -> return Triple(0, 0, emptySet())
            is Verdict.Satisfied -> return Triple(0, 0, verdict.assignment)
            is Verdict.Unresolved -> verdict
        }
        val clauses = unresolved.clauses
        val partialAssignment = unresolved.partialAssignment
        val vars = givenVars.filter { v -> v.toInt().let { it !in partialAssignment || -it !in partialAssignment } }
                .toSet()

        val tries = AtomicInteger()
        val basicAssignment = vars.map(Var::toInt)
        val ret = IntStream.range(0, maxTries).parallel().mapToObj {
            tries.getAndIncrement()
            val assignment = basicAssignment.map { it * (if (random.nextBoolean()) 1 else -1) }.toMutableSet()

            (1..maxFlips).forEach { flip ->
                if (clauses.all { c -> c.any { it in assignment } })
                    return@mapToObj Pair(assignment + partialAssignment, flip)

                val conflictingClause = clauses.find { c -> c.all { it !in assignment } }!!
                var (v, breakCount) = WalkSATCNFSolver.getVarInClauseWithLowestBreakCount(conflictingClause, clauses,
                        assignment)
                if (breakCount != 0 && random.nextFloat() < noise)
                    v = conflictingClause[random.nextInt(conflictingClause.size)].absoluteValue

                assignment.run {
                    if (v in this) {
                        remove(v)
                        add(-v)
                    } else {
                        remove(-v)
                        add(v)
                    }
                }
            }

            null
        }.filter { it != null }.findAny().orElseGet { Pair(emptySet(), maxFlips) }!!

        return Triple(tries.get(), ret.second, ret.first)
    }

    private fun getVarInClauseWithLowestBreakCount(conflictingClause: Clause, clauses: Collection<Clause>,
                                                   assignment: Assignment): Pair<Int, Int> {
        val var2numBreakingClauses = clauses.fold(HashMap<Int, Int>()) { acc, c ->
            conflictingClause.filter { l -> c.all { it == l || it !in assignment } }
                    .forEach { l -> acc[l] = acc.getOrElse(l) { 0 } + 1 }
            acc
        }.toSortedMap()
        val firstKey = var2numBreakingClauses.firstKey()
        val ret = var2numBreakingClauses[firstKey]!!
        return Pair(ret, firstKey)
    }
}

CommandLine().main(args)
